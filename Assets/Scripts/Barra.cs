﻿using UnityEngine;
using System.Collections;

public class Barra : MonoBehaviour {

    float escalaBarra;
    bool terminou;
    bool comecou;
    int score;
    public GameObject cameraCena;

	// Use this for initialization
	void Start () {

        escalaBarra = this.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {

        if (comecou)
        {
            if(escalaBarra > 0)
            {
                DiminuiBarra();
            }
            else
            {
                if (!terminou)
                {
                    terminou = true;
                    cameraCena.SendMessage("FimDeJogo");
                }
            }
        }
	}
    void Comecou()
    {
        comecou = true;
    }
    void DiminuiBarra()
    {
        if (score >= 100)
        {
            escalaBarra = 1.0f;
            terminou = true;
            cameraCena.SendMessage("Win");
        }
        else if (score >= 70)
        {
            escalaBarra = escalaBarra - 0.60f * Time.deltaTime;
            cameraCena.SendMessage("Insane");
        }
        else if (score >= 50)
        {
            escalaBarra = escalaBarra - 0.45f * Time.deltaTime;
            cameraCena.SendMessage("Hard");
        }
        else if(score >= 30)
        {
            escalaBarra = escalaBarra - 0.30f * Time.deltaTime;
            cameraCena.SendMessage("Caution");
        }
        else if(!terminou)
        {
            escalaBarra = escalaBarra - 0.15f * Time.deltaTime;
            cameraCena.SendMessage("Normal");
        }
        this.transform.localScale = new Vector2(escalaBarra, 1.0f);
    }
    void AumentaBarra()
    {
        escalaBarra += 0.10f;
        if(escalaBarra > 1.0f)
        {
            escalaBarra = 1.0f;
        }
    }
    void ScoreUp()
    {
        score++;
    }
    void ScoreDown()
    {
        score -= 5;
    }
}
