﻿using UnityEngine;
using System.Collections;

public class AjustaTela : MonoBehaviour {

    float larguraTela;

    void Start () {

        SpriteRenderer grafico = GetComponent<SpriteRenderer>();

        //Recebe o tamanho da borda da imagem em x
        float larguraImagem = grafico.bounds.size.x;
        //Recebe o tamanho da borda da imagem em y
        float alturaImagem = grafico.bounds.size.y;

        //Recebe o valor da distancia do meio da tela até o topo/bot x 2
        float alturaTela = Camera.main.orthographicSize * 2f;
        larguraTela = alturaTela / Screen.height * Screen.width;

        Vector2 novaEscala = transform.localScale;

        //Encontrar a proporção entre imagem e tela
        novaEscala.x = (larguraTela / larguraImagem) + 0.1f;
        novaEscala.y = (alturaTela / alturaImagem);

        this.transform.localScale = novaEscala;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
