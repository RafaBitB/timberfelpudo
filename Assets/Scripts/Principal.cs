﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class Principal : MonoBehaviour
{

    public GameObject jogadorFelpudo;
    public GameObject felpudoIdle;
    public GameObject felpudoBate;

    public GameObject barril;
    public GameObject inimigoEsq;
    public GameObject inimigoDir;

    public AudioClip somBate;
    public AudioClip somPerde;
    public AudioClip somArremesso;

    float escalaJogadorHorizontal;

    private List<GameObject> listaBlocos;

    bool personagemLadoDirTela;
    bool comecou;
    bool acabou;

    public Text pontuacao;
    public Text nivel;
    int score;

    public GameObject barra;

    // Use this for initialization
    void Start()
    {
        Normal();
        escalaJogadorHorizontal = jogadorFelpudo.transform.localScale.x;
        felpudoBate.SetActive(false);

        listaBlocos = new List<GameObject>();

        criaBarrisInicio(barril);

        pontuacao.transform.position = new Vector2((Screen.width / 2) - 150, (Screen.height / 2) + 50);
        pontuacao.text = "Toque Para Iniciar!";
        nivel.transform.position = new Vector2((Screen.width / 2) + 230, (Screen.height / 2) + 50);

    }

    // Update is called once per frame
    void Update()
    {

        if (!acabou)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (!comecou)
                {
                    comecou = true;
                    barra.SendMessage("Comecou");
                }

                if (Input.mousePosition.x > (Screen.width / 2))
                {
                    bateDireita();
                }
                else
                {
                    bateEsquerda();
                }
                listaBlocos.RemoveAt(0);
                ReposicionaBlocos();
                ConfereJogada();
            }
        }
    }

    private void bateEsquerda()
    {
        personagemLadoDirTela = false;
        StartCoroutine(anima("bate"));
        //pula para o lado esquedo e bate para o lado direito!
        jogadorFelpudo.transform.position = new Vector2(-0.83f, jogadorFelpudo.transform.position.y);
        jogadorFelpudo.transform.localScale = new Vector2(+escalaJogadorHorizontal, jogadorFelpudo.transform.localScale.y);
        StartCoroutine(anima("para"));
        listaBlocos[0].SendMessage("BateEsquerda");
    }

    private void bateDireita()
    {
        personagemLadoDirTela = true;
        StartCoroutine(anima("bate"));
        //pula para o direito esquedo e bate para o lado esquerdo!
        jogadorFelpudo.transform.position = new Vector2(0.83f, jogadorFelpudo.transform.position.y);
        jogadorFelpudo.transform.localScale = new Vector2(-escalaJogadorHorizontal, jogadorFelpudo.transform.localScale.y);
        StartCoroutine(anima("para"));
        listaBlocos[0].SendMessage("BateDireita");
    }
    IEnumerator anima(String acao)
    {
        switch (acao)
        {
            case ("bate"):
                felpudoBate.SetActive(true);
                felpudoIdle.SetActive(false);
                GetComponent<AudioSource>().PlayOneShot(somBate);
                yield return 0;
                break;
            case ("para"):
                //aguarda 0.2 segundo antes de continuar a função;
                yield return new WaitForSeconds(0.2f);
                felpudoBate.SetActive(false);
                felpudoIdle.SetActive(true);
                break;
        }
    }
    GameObject criaNovoBarril(Vector2 posicao)
    {
        GameObject novoBarril;
        //Faz um random entre 0 e 1, se for maior q 0.5 ou a lista não tiver mais de 2 objetos(para que os 2 primeiros barris sejam neutros)
        if (UnityEngine.Random.value > 0.5f || listaBlocos.Count < 2)
        {
            novoBarril = Instantiate(barril);
        }
        else
        {
            if (UnityEngine.Random.value > 0.5f)
            {
                novoBarril = Instantiate(inimigoDir);
            }
            else
            {
                novoBarril = Instantiate(inimigoEsq);
            }
        }

        novoBarril.transform.position = posicao;

        return novoBarril;
    }
    void criaBarrisInicio(GameObject barril)
    {
        for (int i = 0; i <= 7; i++)
        {
            GameObject barrilnovo = criaNovoBarril(new Vector2(0, (barril.transform.position.y + (i * ((barril.transform.localScale.y * 2) + 0.18f)))));
            listaBlocos.Add(barrilnovo);
        }
    }
    void ReposicionaBlocos()
    {
        GameObject barrilnovo = criaNovoBarril(new Vector2(0, (barril.transform.position.y + (8 * ((barril.transform.localScale.y * 2) + 0.18f)))));
        if (!acabou)
        {
            listaBlocos.Add(barrilnovo);
            for (int i = 0; i <= 7; i++)
            {
                listaBlocos[i].transform.position = new Vector2(listaBlocos[i].transform.position.x, listaBlocos[i].transform.position.y - ((barril.transform.localScale.y * 2) + 0.18f));
            }
        }
        

    }
    void ConfereJogada()
    {
        if (listaBlocos[0].gameObject.CompareTag("Inimigo"))
        {
            if ((listaBlocos[0].name == "inimigoEsq(Clone)" && personagemLadoDirTela) || (listaBlocos[0].name == "inimigoDir(Clone)" && !personagemLadoDirTela))
            {
                MarcaPonto();
            }
            else
            {
                PerdePonto();
            }
        }
        else
        {
            MarcaPonto();
        }
    }
    void MarcaPonto()
    {
        score++;
        barra.SendMessage("ScoreUp");
        pontuacao.text = score.ToString();
        pontuacao.fontSize = 100;
        pontuacao.color = new Color(0.95f, 1.0f, 0.35f);
        barra.SendMessage("AumentaBarra");
    }
    void PerdePonto()
    {
        score -= 5;
        barra.SendMessage("ScoreDown");
        if (score < 0)
        {
            FimDeJogo();
        }
        else
        {
            pontuacao.text = score.ToString();
            pontuacao.fontSize = 100;
            pontuacao.color = new Color(1.0f, 0f, 0f);
        }
    }
    void FimDeJogo()
    {
        float lado;

        if (personagemLadoDirTela)
        {
            lado = 5.0f;
        }
        else
        {
            lado = -5.0f;
        }
        acabou = true;
        felpudoBate.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
        felpudoIdle.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);

        jogadorFelpudo.GetComponent<Rigidbody2D>().isKinematic = false;
        jogadorFelpudo.GetComponent<Rigidbody2D>().AddTorque(100f);
        jogadorFelpudo.GetComponent<Rigidbody2D>().velocity = new Vector2(lado, 3.0f);

        pontuacao.text = "PERDEU!";
        GetComponent<AudioSource>().PlayOneShot(somPerde);
        pontuacao.transform.position = new Vector2(Screen.width / 2, pontuacao.transform.position.y);
        nivel.text = "";
        pontuacao.color = new Color(1.0f, 0f, 0f);
        Invoke("RecarregaCena", 2.5f);

    }
    void RecarregaCena()
    {
        Application.LoadLevel("Cena01");
    }
    void Normal()
    {
        inimigoDir.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
        inimigoEsq.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
        barril.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f);
        print("NORMAL");
    }
    void Caution()
    {
        inimigoDir.GetComponent<SpriteRenderer>().color = new Color(0.9f, 0.6f, 0.2f);
        inimigoEsq.GetComponent<SpriteRenderer>().color = new Color(0.9f, 0.6f, 0.2f);
        barril.GetComponent<SpriteRenderer>().color = new Color(0.9f, 0.6f, 0.2f);
        print("CAUTION");
        nivel.text = "CAUTION !";
        nivel.color = new Color(0.95f, 1.0f, 0.35f);
        nivel.fontSize = 50;
    }
    void Hard()
    {
        inimigoDir.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
        inimigoEsq.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
        barril.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.35f, 0.35f);
        print("HARD");
        nivel.text = "HARD !";
        nivel.color = new Color(1.0f, 0.0f, 0.0f);
        nivel.fontSize = 75;
    }
    void Insane()
    {
        inimigoDir.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.10f, 0.5f);
        inimigoEsq.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.10f, 0.5f);
        barril.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.10f, 0.5f);
        print("INSANE");
        nivel.text = "INSANE !";
        nivel.color = new Color(0.4f, 0.0f, 0.8f);
        nivel.fontSize = 100;
    }
    void Win()
    {
        float valor;
        float valorAleatorio;

        foreach (GameObject obj in listaBlocos)
        {
            //valorAleatorio = UnityEngine.Random.value;

            //if(valorAleatorio >= 0.5)
            //{
            //    valor = valorAleatorio * 5f;
            //}
            //else if(valorAleatorio < 0.5)
            //{
            //    valor = valorAleatorio * -5f;
            //}
            //else
            //{
            //    valor = 5f;
            //}

            //obj.GetComponent<Rigidbody2D>().isKinematic = false;
            //obj.GetComponent<Rigidbody2D>().AddTorque(100f);
            //obj.GetComponent<Rigidbody2D>().velocity = new Vector2(valor, 1.5f);
            if(UnityEngine.Random.value > 0.5f)
            {
                obj.SendMessage("BateDireita");
            }
            else
            {
                obj.SendMessage("BateEsquerda");
            }
            GetComponent<AudioSource>().PlayOneShot(somArremesso);
        }
        nivel.text = " VENCEU CARAI! ";
        acabou = true;
        
        Invoke("RecarregaCena",2f);
    }
}
